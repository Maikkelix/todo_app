export class Util {
    static convertISODateToYYMMDDDD(isoDate: string): Date {
        const date = new Date(isoDate);
        return new Date(date.getFullYear(), date.getMonth(), date.getDate());
    }
}