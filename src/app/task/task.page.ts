import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Task } from 'src/task';
import { TodoService } from '../todo.service';
import { Util } from 'src/util';

@Component({
  selector: 'app-task',
  templateUrl: './task.page.html',
  styleUrls: ['./task.page.scss'],
})
export class TaskPage implements OnInit {
  description: string;
  due: string;

  constructor(private location: Location, private todoService: TodoService) { }

  ngOnInit() {
    this.due = new Date().toISOString();
  }

  save() {
    const task = new Task();
    task.description = this.description;
    task.due = Util.convertISODateToYYMMDDDD(this.due);
    this.todoService.addItem(task);
    this.location.back();
  }

}
